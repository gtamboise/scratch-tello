# Introduction

Ce dépot contient des notes rapides sur l'interfacage entre un drone
Ryze Tello et Scratch, en utilisant [Node.js](https://nodejs.org/).

Il est fondé sur [ces instructions](https://www.heliguy.com/blog/2018/04/18/coding-with-the-ryze-tello/)
(et [ce code](https://dl-cdn.ryzerobotics.com/downloads/tello/20180222/Scratch.zip)).

# Composants

![](tello.png)

# Modifications

Vous remarquerez les modifications suivantes à [Tello.js](Tello.js) :
- Récuperation des statuts du drone et stockage dans un fichier CSV
- Réception et pour l'instant uniquement affichage des réponses du drone aux commandes

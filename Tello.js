/*
	Ryze Tello
	Scratch Ext 1.0.0.0
	http://www.ryzerobotics.com
	1/1/2018
*/

/*
	Modifications to ease debugging
	gtamboise@gmail.com
*/

var dataToTrack_keys = ["battery", "x", "y", "z", "speed"];
var lastDataReceived = null;


// Scratch extension (i.e. HTTP server)

const http = require('http');
const fs = require('fs');
const url = require('url');

// We are a client to the Tello Drone command server

const TELLO_PORT = 8889;
const TELLO_HOST = '192.168.10.1';

const dgram = require('dgram');
const client = dgram.createSocket('udp4');

// We want to receive the answers to the commands as well
client.on('listening', () => {
	const address = client.address();
	console.log(`Tello client listening on ${address.address}:${address.port}`);
});
client.on('error', (err) => {
	console.log(`Tello client error:\n${err.stack}`);
	client.close();
});
client.on('message', (msg, rinfo) => {
	console.log(`\nTello client got this response to command: ${msg}`);
});
client.bind(TELLO_PORT)


// We are a server to receive the Tello Drone status
const udp_server = dgram.createSocket('udp4');


http.createServer(function (request, response) {

	var pathname = url.parse(request.url).pathname;
	var url_params = request.url.split('/');

	if (url_params.length < 2)
		return;

	var command = url_params[1];

	switch (command) {

		case 'poll':
			respondToPoll(response);
			break;

		case 'takeoff':
			console.log('takeoff');
			TakeoffRequest();
			break;

		case 'land':
			console.log('land');
			LandRequest();
			break;

		case 'up':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('up ' + dis);
			var message = new Buffer.from('up ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

		case 'down':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('down ' + dis);
			var message = new Buffer.from('down ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

		case 'left':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('left ' + dis);
			var message = new Buffer.from('left ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

		case 'right':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('right ' + dis);
			var message = new Buffer.from('right ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

		case 'forward':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('forward ' + dis);
			var message = new Buffer.from('forward ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

		case 'back':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('back ' + dis);
			var message = new Buffer.from('back ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

		case 'cw':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('cw ' + dis);
			var message = new Buffer.from('cw ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

		case 'flip':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('flip' + dis);
			var message = new Buffer.from('flip ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

		case 'ccw':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('ccw ' + dis);
			var message = new Buffer.from('ccw ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			client.on('message', function (msg, info) {
				console.log('Data received from server : ' + msg.toString());
				console.log('Received %d bytes from %s:%d\n', msg.length, info.address, info.TELLO_PORT);
			});
			break;

		case 'setspeed':
			dis = (url_params.length >= 3) ? url_params[2] : 0;
			console.log('setspeed ' + dis);
			var message = new Buffer.from('speed ' + dis);
			client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
				if (err) throw err;
			});
			break;

	}
	response.end('Hello Tello.\n');

}).listen(8001);


console.log('---------------------------------------');
console.log('Tello Scratch Extension running at http://127.0.0.1:8001/');
console.log('---------------------------------------');


function respondToPoll(response) {

	var noDataReceived = false;

	var resp = "";
	var i;
	for (i = 0; i < dataToTrack_keys.length; i++) {
		resp += dataToTrack_keys[i] + " ";
		resp += (i + 10);
		resp += "\n";
	}
	response.end(resp);
}

function TakeoffRequest() {

	var message = new Buffer.from('command');

	client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
		if (err) throw err;
	});
	var message = new Buffer.from('takeoff');
	client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
		if (err) throw err;

	});
}

function LandRequest() {

	var message = new Buffer.from('land');

	client.send(message, 0, message.length, TELLO_PORT, TELLO_HOST, function (err, bytes) {
		if (err) throw err;
	});
}



// Receiving the UDP status messages from the Tello drone

udp_server.on('error', (err) => {
	console.log(`Tello status server error:\n${err.stack}`);
	server.close();
});

var stream = fs.createWriteStream("DroneStatus.csv", { flags: 'w' });

udp_server.on('message', (msg, rinfo) => {
	stream.write(msg.toString('ascii'));
	process.stdout.write('.');
});

udp_server.on('listening', () => {
	const address = udp_server.address();
	console.log('---------------------------------------');
	console.log(`Tello status server listening on ${address.address}:${address.port}`);
	console.log('---------------------------------------');
});

udp_server.bind(8890);

